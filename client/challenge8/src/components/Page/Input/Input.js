import React from 'react'
import classes from './Input.module.css'

export function Input(props) {
    

    return (
        <div>
            <label>
                <strong>{props.labelFor}</strong>
            </label>
            <br></br>
            <input className={classes.Input}
                type={props.type}
                name={props.name}
                value={props.value}
                onChange={props.change}
                placeholder={`Masukkan ${props.name}`} />  
        </div>
    )
}

export default Input
