import React, { Component } from 'react'
import './Page.css'
import {
    Route,
    NavLink,
    HashRouter
  } from "react-router-dom";

import Home from './Home/Home'
import CreatePlayer from './CreatePlayer/CreatePlayer'
import SearchPlayer from './SearchPlayer/SearchPlayer'
import UpdatePlayer from './UpdatePlayer/UpdatePlayer'

export default class Page extends Component {

  render() {
    return (
      <HashRouter>
        <div>
          <h1  className="Page-header">Player Website</h1>
          <ul className="header">
            <li><NavLink to="/">Home</NavLink></li>
            <li><NavLink to="/register">Register</NavLink></li>
            <li><NavLink to="/update">Update</NavLink></li>
            <li><NavLink to="/search">Search</NavLink></li>
          </ul>
          <div className="content">
            <Route exact path="/" component={Home} />
            <Route exact  path="/register" component={CreatePlayer} />
            <Route exact  path="/update" component={UpdatePlayer} />
            <Route exact  path="/search" component={SearchPlayer} />
          </div>
        </div>
      </HashRouter>
    )
  }
}
