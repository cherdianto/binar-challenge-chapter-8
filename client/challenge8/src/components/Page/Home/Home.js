import React from 'react'

export default function Home(props) {

    return (
        <div>
            <h3>Selamat Datang</h3>
            <p>Ini adalah website client player. Anda dapat mendaftarkan player baru, mengedit player yang telah ada, atau melakukan pencarian pada database player.</p>
            <br></br>
            <p>Selamat Mencoba!</p>
        </div>
    )
}
