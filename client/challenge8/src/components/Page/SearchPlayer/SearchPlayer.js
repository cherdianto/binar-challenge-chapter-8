import React, { Component } from 'react'
import Input from '../Input/Input'

export default class SearchPlayer extends Component {
    constructor(props){
        super(props);
        this.state = {
            username: "",
            email: "",
            exp: "",
            lvl: "",
        }
    }

    onChangeHandler = (event) => { 
        // console.log(event.target);
        this.setState({ 
            [event.target.name] : event.target.value 
        })
    }

    onSubmitHandler = (event) => {
        const { username, email, exp, lvl } = this.state 
        alert(` 
        Your Input :\n 
        Email : ${email} 
        Username : ${username} 
        Experience : ${exp} 
        Level : ${lvl} 
        `) 
    }

    render() {
        return (
            <div>
                <h3>Search Player</h3>
                <p>Anda dapat melakukan pencarian dengan mengisi form di bawah ini!</p>
                <hr></hr>
                <form onSubmit={this.onSubmitHandler}>
                    <Input labelFor="Username" type="username" name="username" change={(event) => this.onChangeHandler(event)} value={this.state.username}/>
                    <Input labelFor="Email" type="email"  name="email" change={(event) => this.onChangeHandler(event)} value={this.state.email}/>
                    <Input labelFor="Experience" type="number"  name="exp" change={(event) => this.onChangeHandler(event)} value={this.state.exp}/>
                    <Input labelFor="Level" type="number"  name="lvl" change={(event) => this.onChangeHandler(event)} value={this.state.lvl}/>
                    <Input type="submit"/>
                </form>
            </div>
        )
    }
}

