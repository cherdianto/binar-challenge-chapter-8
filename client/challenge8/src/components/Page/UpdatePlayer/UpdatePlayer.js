import React, { Component } from 'react'
import Input from '../Input/Input'

export default class UpdatePlayer extends Component {
    constructor(props){
        super(props);
        this.state = {
            username: "",
            email: "",
            exp: "",
            lvl: "",
        }
    }

    onChangeHandler = (event) => { 
        this.setState({ 
            [event.target.name] : event.target.value 
        })
    }

    onSubmitHandler = (event) => {
        const { username, password, email } = this.state 
        alert(` 
        Your Input :\n 
        Email : ${email} 
        Username : ${username} 
        Password : ${password} 
        `) 
    }

    render() {
        return (
            <div>
                <h3>Update Player</h3>
                <p>Anda dapat melakukan penggantian data player dengan mengisi form di bawah ini!</p>
                <hr></hr>
                <form onSubmit={this.onSubmitHandler}>
                    <Input labelFor="Username" type="username" name="username" change={(event) => this.onChangeHandler(event)} value={this.state.username}/>
                    <Input labelFor="Password" type="password" name="password" change={(event) => this.onChangeHandler(event)} value={this.state.password}/>
                    <Input labelFor="Email" type="email"  name="email" change={(event) => this.onChangeHandler(event)} value={this.state.email}/>
                    <Input type="submit"/>
                </form>
            </div>
        )
    }
}


